#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt 
import math
import ising_functions as fn
import plotpbm as pb 




class ising:
	"Class which creates a lattice array, evolves it and calculates the statistical information"
	Temp = 0.1		#starting temp
	Max_Temp = 5.5
	delta_temp = 0.01
	temp_const= int(Max_Temp/delta_temp)
	Temps=np.array([Temp+delta_temp*i for i in range(temp_const)]) #all temps that will be used


	steps_to_equi = 100		#monte carlo time steps until the array is assumed to be in equilibrium
	steps_after_equi = 200	#data will be collected over this many steps after equilibrium is reached



	def __init__(self, dimension, size=10):
		self.energy = np.zeros(self.temp_const)
		self.magnetisation = np.zeros(self.temp_const)
		self.specific=np.zeros(self.temp_const)
		self.suscept=np.zeros(self.temp_const)
		self.d = dimension
		self.n = size
		for i in range(self.temp_const):
			self.initial = fn.initialise_lattice(self.n,self.d)

			#bring lattice to equilibrium
			for j in range(self.steps_to_equi):
				self.initial = fn.update(self.initial,self.Temp,self.n,self.d)

			#gather data
			for k in range(self.steps_after_equi):
				initial= fn.update(self.initial,self.Temp,self.n,self.d)

				self.energy[i]+= fn.mean_energy(self.initial,1,self.d)
				self.magnetisation[i]+= fn.average_magnetisation(self.initial,1,self.d)
				self.specific[i] += fn.mean_energy(self.initial,2,self.d)
				self.suscept[i] += fn.average_magnetisation(self.initial,2,self.d)
			
			self.Temp+=self.delta_temp

		#average the data over the amount of monte carlo time the data was taken over
		self.energy *= 1/float(self.steps_after_equi)
		self.magnetisation *= 1/float(self.steps_after_equi)
		self.specific *= 1/float(self.steps_after_equi)
		self.suscept *= 1/float(self.steps_after_equi)

		#applying the formulae to get the specific heat and the susceptability
		self.specific = (self.specific-(self.energy*self.energy))/(self.Temps*self.Temps)
		self.suscept = (self.suscept-(self.magnetisation*self.magnetisation))/(self.Temps)

	def mean_energy(self):
		plt.plot(self.Temps,self.energy,'r.')
		plt.xlabel("Temperature (J/$k_B$)")
		plt.ylabel("Mean Spin Energy (J, the interaction constant)")
		plt.title("Mean Spin energy vs Temperature")
		plt.show()

	def mean_magnetisation(self):
		plt.plot(self.Temps,self.magnetisation,'r.')
		plt.xlabel("Temperature (J/$k_B$)")
		plt.ylabel("Absolute Value of the Average Spin\n  Magnetisation (J, the interaction constant)")
		plt.title("Absolute Value of the \n Average Spin Magnetisation vs Temperature")
		plt.show()

	def susceptibility(self):
		plt.plot(self.Temps,self.suscept,'r.')
		plt.xlabel("Temperature (J/$k_B$)")
		plt.ylabel("Magnetic Susceptibility")
		plt.title("Magnetic Susceptibility vs Temperature")
		plt.show()

	def specific_heat(self):
		plt.plot(self.Temps,self.specific,'r.')
		plt.xlabel("Temperature (J/$k_B$)")
		plt.ylabel("Specific Heat Capacity (Joules)")
		plt.title("Specific Heat Capacity vs Temperature")
		plt.show()	




#function which plots the magnetisation vs the monte carlo time
#used for interpretting when the lattice is in equilibrium
#equilibrium is when absolute value of the magnetisation is 1 (all spins aligned)
def equi():
	ns=[10,25,100,250] 		#the lattice sizes to be plotted
	steps=[20,70,300,1000]	#the number of time steps to be used for each corresponding n 

	Temp = 1.0 #Temperature used, stays constant
	fig  = plt.figure()
	plt.axis("tight")
	plt.axis("off")
	for i in range(4):
		n=ns[i]
		stepno=steps[i]
		initial = fn.initialise_lattice(n)

		f1 =  fig.add_subplot(2, 2,  i+1) 
		for j in range(stepno):
			initial=fn.update(initial, Temp,n)
			mag = fn.average_magnetisation(initial)

			plt.plot(j,mag,'r.')
			plt.title("Array size is %d by %d" %(n,n))

			#the following is formatting the plot and subplots
			if i==2 or i==3:
				plt.xlabel("Time Steps")
			if i==0 or i==2:
				plt.ylabel("Magnetisation (J)")
			if i==0 or i==1:
				plt.setp(f1.get_xticklabels(), visible=False)
			if i==1 or i==3:
				plt.setp(f1.get_yticklabels(), visible=False)
			plt.axis('tight')
			
	plt.show()







two_dim = ising(2)
two_dim.mean_energy()







'''
#piece of code which evolves a 2d lattice and plots it at certai intervals
Temp=1.4
n=250

initial = fn.initialise_lattice(n)

for i in range(100):
	initial = fn.update(initial,Temp,n)
	if i%50==0:
		X, Y = np.meshgrid(range(n), range(n))
		plt.pcolormesh(X, Y, initial);
		plt.title("Monte Carlo Time = %d" %i)
		plt.show()


X, Y = np.meshgrid(range(n), range(n))
plt.pcolormesh(X, Y, initial);
plt.show()


#function which outputs the lattice array 'initial' as a pbm, saves in the working
#directory
pb.plotpbm(initial,"pbmimagefile.pbm")
'''

