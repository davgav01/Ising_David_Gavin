#!/usr/bin/python

import numpy as np

#function which changes the array of 1s and -1s to 1s and 0s
def array_change(array):
	initial = array
	a,b = initial.shape
	for i in range(b):
		for j in range(a):
			if array[i][j]==-1:
				initial[i][j]=0
	return initial			



#function which takes an array of 1s and -1s and outputs it 
#as a pbm, name by the string 'filename', saves it to
#the working directory
def plotpbm(array,filename):
	a,b = array.shape
	narray=array_change(array)

	f=open(filename,'w')				#filename is the argument passed to the funtion, name of the file
	f.truncate(0)  						#clears the file if it already exists
	f.write("P1 \n")					#format for pbm file
	f.write("# plotpbm module \n")
	string= str(a) + " " +str(b) + "\n"
	f.write(string)

	new_array=narray.astype(bool)		#changes type of the array figures to boolean

	np.savetxt(f,new_array,fmt="%d")	
	f.close()