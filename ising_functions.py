#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt 
import math


#All functions have been expanded to produce results for the
# 1,2 and 3 dimensional cases, corresponding to d=1,2,3
#Also the triangular lattice case with d="tri"

#initialises an n by n matrix of -1s and 1s at random
def initialise_lattice(n,d=2):
	if d==1:
		initial=np.random.randint(0,2,n)	#1 dimension
	elif d==3:
		initial=np.random.randint(0,2,n**3).reshape(n,n,n)	#3-dimensions
	else:	
		initial=np.random.randint(0,2,n**2).reshape(n,n)	#2-dimensions


	initial*=2
	initial-=1
	return initial

#given a point in the 2D array this calculates the energy of the given 
#points using periodic boundary conditions

def position_energy(A,n,a,b,c,d=2,J=1,h=0):
	

	if d==1:
		point = A[a]
		adjacents=A[a-1]+A[(a+1)%n]		#1-dimension
	elif d==3:
		point = A[a,b,c]		#3-dimensions
		adjacents = A[(a+1)%n,b,c] +  A[a-1,b,c]  +  A[a,(b+1)%n,c]  +  A[a,b-1,c] + A[a,b,c-1] + A[a,b,(c+1)%n]
			

	else:
		point = A[a,b]		#2-dimensions
		adjacents =  A[(a+1)%n,b] +  A[a-1,b]  +  A[a,(b+1)%n]  +  A[a,b-1]


	#for d=="tri" this is the triangular lattice which includes 2 extra terms
	if d=="tri":
		adjacents+=A[(a+1)%n,b-1] + A[a-1,b-1]

	total = (-J*point*adjacents)+(h*point)

	return total



#function which updates all the entries of the array 
#according to the metropolis algorithm
def update(A,Temp,n,d=2,J=1,h=0,K_b=1):
	global m
	m=n

	if d==1:
		for i in range(n):
		    total 		= position_energy(A,n,i,1,1,d,J,h)
		    change 		= -2*total
		    random_number = np.random.rand(1)
		    
		    if change < 0:
		    	A[i] *= -1
		    elif random_number < math.exp(-change/(K_b*Temp)):	#boltzmann distribution
		    	A[i] *= -1;

	elif d==3:
		for i in range(n):
			for j in range(n):
				for k in range(n):
				    total 		= position_energy(A,n,i,j,k,d,J,h)
				    change 		= -2*total
				    random_number = np.random.rand(1)
				    
				    if change < 0:
				    	A[i,j,k] *= -1
				    elif random_number < math.exp(-change/(K_b*Temp)):	#boltzmann distribution
				    	A[i,j,k] *= -1;
		    	


	else:
		for i in range(n):
			for j in range(n):
			    total 		= position_energy(A,n,i,j,1,d,J,h)
			    change 		= -2*total
			    random_number = np.random.rand(1)
			    
			    if change < 0:
			    	A[i,j] *= -1
			    elif random_number < math.exp(-change/(K_b*Temp)):	#boltzmann distribution
			    	A[i,j] *= -1;
	return A

#function which calculates the mean energy of the array passed to it
#using the position_energy function defined above
def mean_energy(array,a=1,d=2):	
	global m
	energy = 0
	if d==1:
		for i in range(m):
			energy+=position_energy(array,m,i,1,1,d)	#1-dimension, the 1,1 in the arguments are never used, they
														#can have any value
	
	elif d==3:
		for i in range(m):
			for j in range(m):
				for k in range(m):
				    energy	+= position_energy(array,m,i,j,k,d)	#3-dimensions


	else:
		for i in range(m):
			for j in range(m):
				energy+=(position_energy(array,m,i,j,1,d))	#w-dimension, the 1 in the argument is never used, it
															#can have any value


	if d=="tri":
		d=2
	energy = (energy)/(2.0*(m**d))	#factor of 2 comes from the double counting
	return energy**a

#function which calculates the absolute value of the average spin magnetisation
#of an array. Should be 1 when the system is in equilibrium. 
def net_mag(array):
	net=np.sum(array)
	return net


#calculates and returns the average magnetisation per spin
def average_magnetisation(array,a=1,d=2):
	global m
	mag = net_mag(array)
	if d=="tri":
		d=2			#otherwise it would try and use the string "tri" as an integer
	mag = (mag)/(float(m**d))	
	return (math.fabs(mag))**a


